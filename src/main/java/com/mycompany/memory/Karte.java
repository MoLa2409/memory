package com.mycompany.memory;

import java.awt.Image;

public class Karte {

    private Image image;

    public Karte(Image img) {
        this.image = img;
    }

    public Image getImage() {
        return image;
    }

    
}
