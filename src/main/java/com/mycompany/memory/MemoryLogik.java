package com.mycompany.memory;

import java.util.ArrayList;
import java.util.Random;
import javax.swing.ImageIcon;
import javax.swing.JButton;

public class MemoryLogik {

    private ImageIcon[][] image_array = new ImageIcon[6][6];
    ArrayList<ImageIcon> icon_list = new ArrayList<>();

    public MemoryLogik() {

        ImageIcon icon_1 = new ImageIcon("kaninchen.png");
        icon_list.add(icon_1);
        ImageIcon icon_2 = new ImageIcon("aufladen.png");
        icon_list.add(icon_2);
        ImageIcon icon_3 = new ImageIcon("besteck.png");
        icon_list.add(icon_3);
        ImageIcon icon_4 = new ImageIcon("fahrrad.png");
        icon_list.add(icon_4);
        ImageIcon icon_5 = new ImageIcon("falke.png");
        icon_list.add(icon_5);
        ImageIcon icon_6 = new ImageIcon("flip_flops.png");
        icon_list.add(icon_6);
        ImageIcon icon_7 = new ImageIcon("flugzeug.png");
        icon_list.add(icon_7);
        ImageIcon icon_8 = new ImageIcon("hase.png");
        icon_list.add(icon_8);
        ImageIcon icon_9 = new ImageIcon("marihuana.png");
        icon_list.add(icon_9);
        ImageIcon icon_10 = new ImageIcon("osterei.png");
        icon_list.add(icon_10);
        ImageIcon icon_11 = new ImageIcon("polizei.png");
        icon_list.add(icon_11);
        ImageIcon icon_12 = new ImageIcon("puppe.png");
        icon_list.add(icon_12);
        ImageIcon icon_13 = new ImageIcon("regen.png");
        icon_list.add(icon_13);
        ImageIcon icon_14 = new ImageIcon("regnerisch.png");
        icon_list.add(icon_14);
        ImageIcon icon_15 = new ImageIcon("schwein.png");
        icon_list.add(icon_15);
        ImageIcon icon_16 = new ImageIcon("sonne.png");
        icon_list.add(icon_16);
        ImageIcon icon_17 = new ImageIcon("sonne_1.png");
        icon_list.add(icon_17);
        ImageIcon icon_18 = new ImageIcon("vogelscheuche.png");
        icon_list.add(icon_18);

        Random r = new Random();

        boolean prüf = true;
        int anzahl_bilder = 0;

        while (prüf) {

            for (int i = 0; i < 18; i++) {
                for (int j = 0; j < 2; j++) {
                    int pos1 = r.nextInt(6);
                    int pos2 = r.nextInt(6);

                    if (image_array[pos1][pos2] == null) {
                        image_array[pos1][pos2] = icon_list.get(i);
                        anzahl_bilder = anzahl_bilder + 1;
                    }
                    if (anzahl_bilder == 36) {
                        prüf = false;
                    }
                }
            }

        }

    }

    public void cheat() {
        int[][] jaja = new int[6][6];
        for (int i = 0; i < jaja.length; i++) {
            for (int j = 0; j < jaja.length; j++) {
                if (image_array[i][j] != null) {
                    jaja[i][j] = 1;
                }
                System.out.print(jaja[i][j]);
            }
            System.out.println();
        }
    }

    public void feld_aufdecken(JButton[][] spielfeld, int x, int y) {
        spielfeld[x][y].setIcon(image_array[x][y]);
    }
}
