package com.mycompany.memory;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;

public class MemoryGUI extends JPanel implements ActionListener {

    private JButton[][] spielfeld = new JButton[6][6];

    MemoryLogik ML = new MemoryLogik();

    public MemoryGUI() {
        setLayout(null);

        int h = 10;

        for (int i = 0; i < spielfeld.length; i++) {

            int w = 20;
            for (int j = 0; j < spielfeld.length; j++) {

                spielfeld[i][j] = new JButton();
                spielfeld[i][j].setBounds(w, h, 60, 60);
                spielfeld[i][j].setBackground(Color.white);
                spielfeld[i][j].setOpaque(true);
                spielfeld[i][j].setBorder(BorderFactory.createLineBorder(Color.black, 2));
                spielfeld[i][j].addActionListener(this);
                this.add(spielfeld[i][j]);
                w = w + 70;
            }
            h = h + 70;
        }
        ML.cheat();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        for (int i = 0; i < spielfeld.length; i++) {
            for (int j = 0; j < spielfeld.length; j++) {
                if (e.getSource().equals(spielfeld[i][j])) {
                    
                    ML.feld_aufdecken(spielfeld, i, j);
                }
            }

        }
    }
}
