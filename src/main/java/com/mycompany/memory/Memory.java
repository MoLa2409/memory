package com.mycompany.memory;

import java.awt.Color;
import javax.swing.JFrame;

public class Memory {

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        frame.setSize(470, 500);
        frame.setTitle("  Memory");
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);

        MemoryGUI gui = new MemoryGUI();

        frame.setContentPane(gui);

        frame.getContentPane().setBackground(Color.gray);

        frame.setVisible(true);
    }
}
